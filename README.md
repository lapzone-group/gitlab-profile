# LapZone Group

Welcome to the LapZone GitLab group! This space is dedicated to the "LapZone" Internet shop project. 

## Demo

Click **<a href="https://lapzone.tech" target="_blank">here</a>** to open LapZone internet shop.

_Below are the repositories associated with this group:_

## 1. [LapZone](https://gitlab.com/lapzone-group/LapZone)
**Description:**
The `LapZone` repository is the core of our project, containing the Django Internet shop for selling laptops and accessories. Explore and customize the shop to meet your specific needs.

## 2. [DRF](https://gitlab.com/lapzone-group/DRF)
**Description:**
The `DRF` repository focuses on the API component of the "LapZone" Internet shop. Built with Django and Django Rest Framework (DRF), this API supports seamless data interaction and integration with the shop.

## 3. [Vue-frontend](https://gitlab.com/lapzone-group/Vue-frontend)
**Description:**
The `Vue-frontend` repository houses the frontend component with Vue.js for the LapZone API (Django + DRF). This frontend is designed to provide an intuitive and dynamic user interface for an enhanced shopping experience.

> This README.md generated with assistance from ChatGPT
